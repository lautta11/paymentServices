// db.js
const  mongoose  = require('mongoose');
// Load environment variables
require("../loadEnvironment.js");

const connectionString = process.env.ATLAS_URI || "";

module.exports = async function connectToMongo() {
  try {
      await mongoose.connect(connectionString, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      console.log('Connected to MongoDB');
      const db = mongoose.connection;
      return db;
  } catch (error) {
    console.error("Failed to connect to MongoDB:", error);
    throw error;
  }
}
