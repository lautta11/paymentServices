const mongoose = require('mongoose');
const commerceSchema = new mongoose.Schema({
  name: { type: String, required: true },
  username: { type: String, required: true },
  password: { type: String, required: true },
  cuil: { type: String, required: false },
});

const Commerce = mongoose.model('Commerce', commerceSchema);
module.exports =  {Commerce}
