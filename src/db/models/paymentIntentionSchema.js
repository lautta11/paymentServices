const mongoose = require('mongoose');
const paymentIntentionSchema = new mongoose.Schema({
  commerce: { type: mongoose.Schema.Types.ObjectId, ref: 'Commerce', required: true },
  product: { type: String, required: true },
  price: { type: Number, required: true },
  blocked: { type: Boolean, default: false },
  paymentAttempts: { type: Number, default: 0 },
});

const PaymentIntention = mongoose.model('PaymentIntention', paymentIntentionSchema);

module.exports =  {PaymentIntention}
