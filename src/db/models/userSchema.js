const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({
  username: { type: String, required: true },
  password: { type: String, required: true },
  email: { type: String, required: true },
  dni: { type: String, required: false },
  creditCard: { 
    number: { type: String, required: true },
    expiration: { type: Number, required: true },
    limit: { type: Number, required: true },
    debt: { type: Number, required: true,  default: 0 }
  }
});

const User = mongoose.model('User', userSchema);

module.exports =  {User, userSchema};
