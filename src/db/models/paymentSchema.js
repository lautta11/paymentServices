const mongoose = require('mongoose');
const paymentSchema = new mongoose.Schema({
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  paymentIntention: { type: mongoose.Schema.Types.ObjectId, ref: 'PaymentIntention', required: true },
  status: { type: String, enum: ['pending', 'approved', 'failed'], default: 'pending' },
  reason: { type: String, required: false },
});

const Payment = mongoose.model('Payment', paymentSchema);

module.exports={Payment};
