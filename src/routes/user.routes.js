const express = require("express")
const { validateUser, validateUserId } = require("../middlewares/validators/validators.js");
const { postCreateUser, getUsers, getUserById, updateUser, deleteUser } = require("../controllers/user.controller.js");
const { verifyToken } = require("../middlewares/auth/verifyToken.js");
// Load environment variables
require("../loadEnvironment.js");


const userRouter = express.Router();


// Create a user
userRouter.post('/',[validateUser], postCreateUser);

// Get all users

userRouter.get('/',[verifyToken,validateUserId], getUsers);

// Get a user by ID
userRouter.get('/:id',[verifyToken,validateUserId], getUserById);

// Update a user
userRouter.put('/:id',[verifyToken,validateUserId], updateUser);

// Delete a user
userRouter.delete('/:id',[verifyToken,validateUserId], deleteUser);



module.exports = { userRouter };
