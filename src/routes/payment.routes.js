// Load environment variables
require("../loadEnvironment.js");
const express = require("express")
const  {  validatePaymentIntention, validateUserId, validateCommerceId, validatePayment } = require('../middlewares/validators/validators.js');
const  { createPayment, createPaymentIntentions, getPaymentIntentions, getPaymentsById, getAllPayments, getPaymentIntentionsById } = require('../controllers/payment.controller.js');
const { verifyToken } = require("../middlewares/auth/verifyToken.js");


const paymentRouter = express.Router();

paymentRouter.get('/', [verifyToken,validateUserId],getAllPayments)

paymentRouter.get('/:id',[verifyToken,validateUserId], getPaymentsById)

// Process a payment
// Only user can create a payment
paymentRouter.post('/',[verifyToken,validateUserId, validatePayment],createPayment);

// Payment intention model
// Only commerce  can create a payment intention
paymentRouter.post('/paymentIntention', [verifyToken,validateCommerceId,validatePaymentIntention] , createPaymentIntentions);


paymentRouter.get('/paymentIntention', getPaymentIntentions);


paymentRouter.get('/paymentIntention/:id', getPaymentIntentionsById);



module.exports = { paymentRouter };