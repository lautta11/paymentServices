const express  = require('express');
const { postCommerce, getCommerce, getCommerceById, putCommerce, deleteCommerce } = require('../controllers/commerce.controller.js');
const { verifyToken } = require('../middlewares/auth/verifyToken.js');
const { validateCommerce, validateCommerceId } = require('../middlewares/validators/validators.js');
// Load environment variables
require("../loadEnvironment.js");

const commerceRouter = express.Router();

// Create a commerce
// TODO: create test
commerceRouter.post('/',[validateCommerce],postCommerce);

// Get all commerce
commerceRouter.get('/',[verifyToken,validateCommerceId],getCommerce);

// Get a commerce by ID
commerceRouter.get('/:id',[verifyToken,validateCommerceId],getCommerceById);

// Update a commerce
commerceRouter.put('/:id',[verifyToken,validateCommerceId],putCommerce);

// Delete a commerce
commerceRouter.delete('/:id',[verifyToken,validateCommerceId], deleteCommerce);




module.exports =  { commerceRouter };
