const express = require("express")
const { User } = require('../db/models/userSchema.js');
const jwt = require('jsonwebtoken');
const { Commerce } = require("../db/models/commerceSchema.js");
// Load environment variables
require("../loadEnvironment.js");


const authRouter = express.Router();


// Auth
authRouter.post('/user/login', async (req, res) => {
    const { username, password } = req.body;
  
    try {
      const user = await User.findOne({ username });
  
      if (!user || password !== user.password) {
        return res.status(401).json({ message: 'Invalid credentials' });
      }
  
      // Generate JWT token
      const token = jwt.sign({ userId: user._id }, process.env.JWT_SECRET, { expiresIn: '1h' });
  
      return res.json({ message: 'Login successful', token });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: 'Internal server error' });
    }
});

authRouter.post('/commerce/login', async (req, res) => {
    const { username, password } = req.body;
  
    try {
      const commerce = await Commerce.findOne({ username });
  
      if (!commerce || password !== commerce.password) {
        return res.status(401).json({ message: 'Invalid credentials' });
      }
  
      // Generate JWT token
      const token = jwt.sign({ commerceId: commerce._id }, process.env.JWT_SECRET, { expiresIn: '1h' });
  
      return res.json({ message: 'Login successful', token });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: 'Internal server error' });
    }
  });

  
module.exports = {
    authRouter
}