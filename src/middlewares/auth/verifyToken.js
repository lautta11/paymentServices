const jwt = require('jsonwebtoken');
require("../../loadEnvironment")
// Middleware function to verify the JWT token
const verifyToken = (req, res, next) => {
  let token = req.headers.authorization;

  if (!token) {
    return res.status(401).json({ message: 'Access denied. Token missing.' });
  }

  try {
    token = token.split(' ')[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    req.userId = decoded.userId; // For user endpoints
    req.commerceId = decoded.commerceId; // For commerce endpoints
    next();
  } catch (error) {
    return res.status(401).json({ message: 'Invalid token.' });
  }
};

module.exports = {
  verifyToken
}