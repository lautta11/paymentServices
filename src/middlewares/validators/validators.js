const   { body } = require ('express-validator');

 const validateUser = [
        body('username').notEmpty().withMessage('Username is required'),
        body('password').notEmpty().withMessage('Password is required'),
        body('email').isEmail().withMessage('Invalid email'),
        body('creditCard').notEmpty().withMessage('Credit card is required'),
        body('creditCard').isObject({
          strict: true,
          checkRequired: true,
          checkProperties: true,
          required: true,
          properties: {
            number: {
              type: 'string',
              required: true
            },
            expiration: {
              type: 'number',
              required: true
            },
            limit:{
              type: 'number',
              required: true
            },
            debt:{
              required: true,
              type: 'number'
            }
          }
        }).withMessage('Credit card invalid')
        
  ]

  /**
   *  name,
        username,
        password,
        cuil,
   * 
   */
  const validateCommerce = [
    body('username').notEmpty().withMessage('Username is required'),
    body('password').notEmpty().withMessage('Password is required'),
    body('cuil').notEmpty().withMessage('Cuil is required'),
  ]

 const  validatePaymentIntention = [
    body('commerce').notEmpty().isString().withMessage('Commerce is required'),
    body('product').notEmpty().withMessage('Product is required'),
    body('price').isNumeric().withMessage('Price must be a number')
  ]


 const validatePayment = [
    body('paymentIntentionId').notEmpty().withMessage('Payment intention id is required'),
    body('userId').notEmpty().withMessage('User id is required')
  ]



const validateCommerceId = (req, res, next) => {
  const isCommerce = req.commerceId !== undefined;
  if (!isCommerce) {
    return res.status(401).json({ message: 'Unauthorized' });
  }
  next();
};
const validateUserId = (req, res, next) => {
  const isUser = req.userId !== undefined;
  if (!isUser) {
    return res.status(401).json({ message: 'Unauthorized' });
  }
  next();
};


module.exports = {
  validateUser,
  validateCommerce,
  validatePaymentIntention,
  validatePayment,
  validateCommerceId,
  validateUserId
}