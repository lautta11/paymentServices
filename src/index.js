// index.js
const express = require("express")
const connectToMongo = require("./db/conn.js") ;
const  {userRouter} = require("./routes/user.routes.js");
const  {commerceRouter} = require("./routes/commerce.routes.js");
const { paymentRouter } = require("./routes/payment.routes.js");
const { authRouter } = require("./routes/auth.routes.js");

const app = express();

// For parse json in comming request
app.use(express.json());

// Middleware to set up the db instance
app.use(async (req, res, next) => {
  try {
    req.db = await connectToMongo();
    next();
  } catch (error) {
    console.error("Failed to connect to MongoDB:", error);
    res.status(500).json({ error: "Failed to connect to MongoDB" });
  }
});

app.use("/user", userRouter);
app.use("/commerce", commerceRouter);
app.use("/payment", paymentRouter);
app.use("/auth", authRouter)

// Other app configurations and middleware...

// Start the server
app.listen(3000, () => {
  console.log("Server started on port 3000");
});
