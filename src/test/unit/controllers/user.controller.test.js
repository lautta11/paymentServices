const { postCreateUser } = require("../../../controllers/user.controller");
const { User } = require("../../../db/models/userSchema");


jest.mock("../../../db/models/userSchema");

describe("UserController", () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
  
    describe("postCreateUser", () => {
      it("should create a new user and return success message", async () => {
        const req = {
          body: {
            username: "testuser",
            password: "testpassword",
            email: "test@test.com",
            dni: "1234567890",
            creditCard: {
                number:"454587552100",
                expiration: 1641265200000.0,
                limit:100000,
                debt:0
            }
          },
        };
        const res = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn(),
        };

        // Mock Coomerce model methods
        const saveUserMock = jest
        .fn()
        .mockResolvedValueOnce({
        "_id": { "$oid": "12345" },
        "name": "Test Commerce" ,
        "username": "testuser",
        "password": "testpassword",
        "email": "test@test.com",
        "dni": "1234567890",
        "creditCard": {
           "number":"454587552100",
           "expiration":{"$numberDouble":"1641265200000.0"},
           "limit":{"$numberInt":"100000"},
           "debt":{"$numberInt":"0"}
        }
           ,"__v":{"$numberInt":"0"}
        });

        User.mockImplementationOnce(() => ({
            save: saveUserMock,
        }));

  
        await postCreateUser(req, res);
  
        expect(User).toHaveBeenCalledWith({
          username: "testuser",
          password: "testpassword",
          email: "test@test.com",
          dni: "1234567890",
          creditCard:{
            number:"454587552100",
            expiration: 1641265200000.0,
            limit:100000,
            debt:0
        },
        });
        expect(res.status).toHaveBeenCalledWith(201);
      });
  
    });
})