const { postCommerce } = require("../../../controllers/commerce.controller");
const { Commerce } = require("../../../db/models/commerceSchema");

jest.mock("../../../db/models/commerceSchema")
  
  describe("CommerceController", () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
  
    describe("postCommerce", () => {
      it("should create a new commerce and return success message", async () => {
        const req = {
          body: {
            name: "Test Commerce",
            username: "testuser",
            password: "testpassword",
            cuil: "1234567890",
          },
        };
        const res = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn(),
        };


        // Mock Coomerce model methods
        const saveCommerceMock = jest
        .fn()
        .mockResolvedValueOnce({
        "_id": { "$oid": "12345" },
        "name": "Test Commerce" ,
        "username": "testuser",
        "password": "testpassword",
        "cuil": "1234567890",
        "__v": { "$numberInt": "0" },
        });

        Commerce.mockImplementationOnce(() => ({
            save: saveCommerceMock,
        }));
  
        await postCommerce(req, res);
  
        expect(Commerce).toHaveBeenCalledWith({
          name: "Test Commerce",
          username: "testuser",
          password: "testpassword",
          cuil: "1234567890",
        });
        expect(res.status).toHaveBeenCalledWith(201);
        expect(saveCommerceMock).toHaveBeenCalled();
      });
  
    });
  
  })