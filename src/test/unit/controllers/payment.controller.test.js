
const { PaymentIntention } = require('../../../db/models/paymentIntentionSchema.js');
const { User } = require('../../../db/models/userSchema.js');
const { Payment } = require('../../../db/models/paymentSchema.js');
const { createPayment, createPaymentIntentions, getPaymentIntentions } = require('../../../controllers/payment.controller.js');
const { default: mongoose } = require('mongoose');
const axios = require('axios');

jest.mock('../../../db/models/paymentIntentionSchema.js');
jest.mock('../../../db/models/userSchema.js');
jest.mock('../../../db/models/paymentSchema.js');
jest.mock("axios");


describe('Payment Controller', () => {
  describe('createPayment', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    test('should create a payment successfully', async () => {



      // Mock request and response objects
      const req = {
        body: {
          "paymentIntentionId": "645d5b3659f5209563908a22",
          "userId": "645d3e8cdf05a546a34177d7",
        }
      };
      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      // Mock PaymentIntention model methods
      PaymentIntention.findById.mockResolvedValueOnce({
        /* mock payment intention data */
        "_id": new mongoose.Types.ObjectId("645d5b3659f5209563908a22"),
        "commerce": "645d4aa6eec7a7b057a3532f",
        "product": "auto",
        "price": 100000,
        "paymentAttempts": 1,
        "__v": 0,
        "blocked": false
      });

      PaymentIntention.updateOne.mockResolvedValueOnce({
        "_id": { "$oid":"645d5b3659f5209563908a22"},
        "commerce": "645d4aa6eec7a7b057a3532f",
        "product": "auto",
        "price": 100000,
        "paymentAttempts": 3,
        "__v": 0,
        "blocked": false
      })


      // Mock User model methods
      User.findById.mockResolvedValueOnce(
        /* mock user data with valid credit card */
        {
          "_id": new mongoose.Types.ObjectId("645d3e8cdf05a546a34177d7"),
          "username": "lautaro",
          "password": "123",
          "email": "lautarov@gmail.com",
          "dni": "36655569",
          "creditCard":
          {
            "number": "454587552100",
             "expiration":1641265200000.0,
            "limit": 100000,
            "debt": 0
          },
          "__v": { "$numberInt": "0" }
        }
      );


  // Mock Payment model methods
  const savePaymentMock = jest
    .fn()
    .mockResolvedValueOnce({
      "_id": { "$oid": "645db8d310f67b99e1aeb30c" },
      "user": { "$oid": "645d3e8cdf05a546a34177d7" },
      "paymentIntention": { "$oid": "645d5b3659f5209563908a22" },
      "status": "failed",
      "reason": "credit card not valid",
      "__v": { "$numberInt": "0" },
    });

  Payment.mockImplementationOnce(() => ({
    save: savePaymentMock,
  }));

  // const {creditCard} = 
  // // Mock validateCreditCard function
  // validateCreditCard({creditCard,PaymentIntention}).mockReturnValueOnce(true);

  // Mock sendCallback function
  axios.post.mockResolvedValueOnce();

  // Call the controller function
  await createPayment(req, res);

  // Check the response
  expect(res.status).toHaveBeenCalledWith(200);
  expect(res.json).toHaveBeenCalledWith({ Payment: expect.any(Object) });

  // Check the function calls
  expect(PaymentIntention.findById).toHaveBeenCalledWith(new mongoose.Types.ObjectId('645d5b3659f5209563908a22'));
  expect(User.findById).toHaveBeenCalledWith(new mongoose.Types.ObjectId('645d3e8cdf05a546a34177d7'));
  expect(Payment).toHaveBeenCalledWith({
    "paymentIntention": "645d5b3659f5209563908a22",
    "status": "pending",
    "user": "645d3e8cdf05a546a34177d7"
  });
  expect(savePaymentMock).toHaveBeenCalled();
});

    // Add more test cases for different scenarios (e.g., invalid payment intention, user with invalid credit card, etc.)
  });

  describe('createPaymentIntentions', () => {
    // Add tests for the createPaymentIntentions function
    describe('createPaymentIntentions', () => {
      beforeEach(() => {
        jest.clearAllMocks();
      });

      test('should create a payment intention successfully', async () => {
        // Mock request and response objects
        const req = {
          body: {
            commerce: '645d4e55eec7a7b057a35331',
            product: 'tele',
            price: 130000,
          },
        };
        const res = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn(),
        };

  
          // Mock Payment model methods
            const savePaymentIntentionMock = jest
            .fn()
            .mockResolvedValueOnce({
              "_id": { "$oid": "645db8d310f67b99e1aeb30c" },
              "commerce": { "$oid": "645d3e8cdf05a546a34177d7" },
              "product": { "$oid": "645d5b3659f5209563908a22" },
              "price": "failed",
              "paymentAttempts": "credit card not valid",
              "__v": { "$numberInt": "0" },
            });

            PaymentIntention.mockImplementationOnce(() => ({
            save: savePaymentIntentionMock,
          }));

        // Call the controller function
        await createPaymentIntentions(req, res);

        // Check the response
        expect(res.status).toHaveBeenCalledWith(201);

        // Check the function calls
        //expect(validationResult).toHaveBeenCalledWith(req);
        expect(savePaymentIntentionMock).toHaveBeenCalled();
      });

  });
  });

  describe('getPaymentIntentions', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    test('should get payment intentions successfully', async () => {
      // Mock response object
      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      // Mock PaymentIntention.find method
      const paymentIntentions = [
        {
          _id: '645d5b3659f5209563908a22',
          commerce: '645d4aa6eec7a7b057a3532f',
          product: 'Naranja',
          price: 100000,
          paymentAttempts: 2,
        },
        {
          _id: '645d5b3659f5209563908a23',
          commerce: '645d4e55eec7a7b057a35331',
          product: 'MercadoLibre',
          price: 20000,
          paymentAttempts: 3,
        },
      ];
      PaymentIntention.find.mockResolvedValueOnce(paymentIntentions);

      // Call the controller function
      await getPaymentIntentions(null, res);

      // Check the response
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith({ paymentIntentions });

      // Check the function call
      expect(PaymentIntention.find).toHaveBeenCalled();
    });
  });

})
