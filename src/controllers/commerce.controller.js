const { validationResult } = require("express-validator");
const { Commerce } = require("../db/models/commerceSchema");


const postCommerce = async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
  
      const { name, username, password, cuil } = req.body;
  
      const newCommerce = new Commerce({
        name,
        username,
        password,
        cuil,
      });
  
      await newCommerce.save();
  
      res.status(201).json({ message: 'Commerce created successfully', commerce: newCommerce });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Internal server error' });
    }
}

const getCommerce = async (req, res) => {

    try {
      const commerceList = await Commerce.find();
      res.json(commerceList);
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Internal server error' });
    }
}

const getCommerceById =  async (req, res) => {
    try {
      const commerce = await Commerce.findById(req.commerceId);
      if (!commerce) {
        return res.status(404).json({ message: 'Commerce not found' });
      }
      res.json(commerce);
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Internal server error' });
    }
}

const putCommerce = async (req, res) => {

    try {
      const errors = validationResult(req.commerceId);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
  
      const { name, username, password, cuil } = req.body;
  
      const commerce = await Commerce.findByIdAndUpdate(
        req.params.id,
        { name, username, password, cuil },
        { new: true }
      );
      
      if (!commerce) {
        return res.status(404).json({ message: 'Commerce not found' });
      }
  
      res.json({ message: 'Commerce updated successfully', commerce });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Internal server error' });
    }
  }

const deleteCommerce = async (req, res) => {

    try {
      const commerce = await Commerce.findByIdAndDelete(req.commerceId);
      if (!commerce) {
        return res.status(404).json({ message: 'Commerce not found' });
      }
      res.json({ message: 'Commerce deleted successfully', commerce });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Internal server error' });
    }
  }

module.exports = {
    postCommerce,
    getCommerce,
    getCommerceById,
    putCommerce,
    deleteCommerce
}