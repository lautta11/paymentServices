// Load environment variables
require("../loadEnvironment.js");
const mongoose = require('mongoose');
const { validationResult } = require ('express-validator');
const { PaymentIntention } = require ('../db/models/paymentIntentionSchema.js');
const { User } = require( '../db/models/userSchema.js');
const { Payment } = require( '../db/models/paymentSchema.js');
const axios = require('axios');


 const createPayment = async (req, res) => {

    try {
        
        // Validate request body
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        // Extract data from request body
        const { paymentIntentionId, userId } = req.body;

        // Find the payment intention in the database
        const paymentIntention = await PaymentIntention.findById(new mongoose.Types.ObjectId(paymentIntentionId));
        if (!paymentIntention) {
            return res.status(404).json({ message: 'Payment intention not found' });
        }

        // Check if payment intention has reached the maximum number of payment attempts
        if (paymentIntention.paymentAttempts >= 3) {
            paymentIntention.blocked = true; // Block the payment intention
            return res.status(400).json({ message: 'Payment intention has reached the maximum number of payment attempts' });
        }


        // Find the user in the database
        const user = await User.findById(new mongoose.Types.ObjectId(userId));

        // Prepare payment data

        const payment = new Payment({
            paymentIntention: paymentIntentionId,
            user: userId,
            status: 'pending',
        });

        // Validate credit card and check available limit and debt
        const { creditCard } = user;
        if (!validateCreditCard({ creditCard, paymentIntention })) {
            payment.status = 'failed';
            payment.reason = 'credit card not valid'
        }else{
            // save payment
            payment.status = 'approved';
            paymentIntention.blocked = true; // Block the payment intention
        }
        
        paymentIntention.paymentAttempts += 1;
        
        
        
        
        // Save payment intention
        await PaymentIntention.updateOne(paymentIntention);
        await payment.save();
        // Send callback to user's and commerce's specified URLs
        const callbackPayload = { paymentIntention, payment};
        await sendCallback(process.env.WEBHOOK_URI || "", callbackPayload);


       
        res.status(200).json({ Payment: payment });
    } catch (error) {

        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
}

 const createPaymentIntentions =  async (req, res) => {
    
    try {
        // Validate request body
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        // Extract data from request body
        const { commerce, product, price } = req.body;

        // Create a new payment intention
        const newPaymentIntention = new PaymentIntention({
            commerce,
            product,
            price,
            paymentAttempts: 0,
        });

        // Save the payment intention
        await newPaymentIntention.save();

        res.status(201).json({ message: 'Payment intention created successfully', paymentIntention: newPaymentIntention });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
}

const getPaymentIntentions = async (req, res) => {
    try {
        const paymentIntentions = await PaymentIntention.find()
        res.status(200).json({ paymentIntentions });
    }
    catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
}

const getPaymentIntentionsById = async (req, res) => {
    try {
        const paymentIntentions = await PaymentIntention.findOne(req.params.id)
        res.status(200).json({ paymentIntentions });
    }
    catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
}

const getAllPayments = async (req, res) => {
    try {
        const payments = await Payment.find()
        res.status(200).json({ payments });
    }catch (error) {
        res.status(500).json({ message: 'Internal server error' });
    
}}

const getPaymentsById = async (req, res) => {

    try {
        const payment = await Payment.findById(new mongoose.Types.ObjectId(req.params.paymentId));
        res.status(200).json({ payment });
    
    } catch (error) {
        res.status(500).json({ message: 'Internal server error' });
    }
    
}

/**
 *  
 * @param {*} creditCard 
 */
 const validateCreditCard = ({creditCard, paymentIntention}) => {
    const { expiration, limit, debt } = creditCard
    const { price } = paymentIntention

    const isActive = expiration > Date.now()
    const availableLimit = limit - debt

    // Validation logic for credit card limit
    //if (!creditCard.active || debt > 0 || availableLimit < paymentIntention.price) 
    return ( isActive && availableLimit >= price)
    // Return true or false based on the validation result
  };

  // Function to send callback to a specified URL
async function sendCallback(url, payload) {
    try {
        await axios.post(url, payload);
    } catch (error) {
        console.error(`Failed to send callback to URL: ${url}`, error);
    }
}

module.exports = {
    createPayment,
    createPaymentIntentions,
    validateCreditCard,
    getPaymentIntentions,
    getPaymentsById,
    getAllPayments,
    getPaymentIntentionsById
}